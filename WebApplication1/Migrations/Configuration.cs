namespace WebAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebAPI.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebAPI.Models.Datum>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebAPI.Models.Datum context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
         /*   context.Cells.AddOrUpdate(x => x.Id, new CellDTO() { Id = 1, Voltage = 11.8f, Balance = 12.8f, Temp = 13.8f },
                new Cell() { Id = 2, Voltage = 21.8f, Balance = 22.8f, Temp = 23.8f },
                new Cell() { Id = 3, Voltage = 31.8f, Balance = 32.8f, Temp = 33.8f });
    */    
    }
    }
}
