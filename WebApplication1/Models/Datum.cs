﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class Datum : DbContext
    {
        public Datum() : base("name=Datum")
        {
            this.Database.Log = s=> System.Diagnostics.Debug.WriteLine(s);
        }

        public System.Data.Entity.DbSet<WebAPI.Models.CellDTO> Cells { get; set;}
    }
}