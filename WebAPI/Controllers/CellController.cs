﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI
{
    public class CellController : ApiController
    {
        private Datum db = new Datum();
        // GET api/<controller>
        [Route ("api/Cell")]
        [System.Web.Http.AcceptVerbs("GET")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(CellDTO))]
        public IEnumerable<CellDTO> GetCells()
        {
            var cells = from c in db.Cells
                        select new CellDTO()
                        {
                            Id = c.Id,
                            Voltage = c.Voltage,
                            Balance = c.Balance,
                            Temp = c.Temp
                        };
            return cells;
        }

        // GET api/<controller>/5
        //  [ResponseType(typeof(CellDetailDTO))]
        /*  public async Task<IHttpActionResult> GetCell(int id)
          {
              var cell = await db.Cells.Include(c => c.).Select(c => new CellDetailDTO()
              { Id = new c.Id,

              }).SingleOrDefaultAsync(c => c.ID == id);

                  if(cell == null)
              {
                  return NotFound();
              }
              return Ok(cell);
      }*/

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}