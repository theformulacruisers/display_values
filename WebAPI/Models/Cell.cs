﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class Cell
    {
        public int Id { get; set; }
        public float Voltage { get; set; }
        public float Balance { get; set; }
        public float Temp { get; set; }
    }
}