﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebApplication.Hubs
{
    public class CriticalHub : Hub
    {
        // Create the instance of ChartDataUpdate
        private readonly CriticalDataUpdate _ChartInstance;
        public CriticalHub() : this(CriticalDataUpdate.Instance) { }

        public CriticalHub(CriticalDataUpdate ChartInstance)
        {
            _ChartInstance = ChartInstance;
        }

        public void InitChartData()
        {
            //Show Chart initially when InitChartData called first time
            LineChart lineChart = new LineChart();
            string []array= { "0" };
            lineChart.SetLineChartData(array);
            Clients.All.InitChart(SettingsConfig.settings.criticalHub.charts);

            //Call GetChartData to send Chart data every 5 seconds
            _ChartInstance.GetChartData(SettingsConfig.settings.criticalHub.updatePeriod);

        }
    }
}