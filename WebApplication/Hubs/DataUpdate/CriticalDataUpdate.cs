﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using System.IO;

namespace WebApplication
{
    //The Line Chart Class
    public class LineChart
    {
        [JsonProperty("lineChartData")]
        private int[] lineChartData;
        [JsonProperty("colorString")]
        private string colorString;

        public void SetLineChartData(string [] value)
        {
            lineChartData = new int[7];
            lineChartData[0] = Int32.Parse(value[0]);
          /*  lineChartData[0] = RandomNumberGenerator.randomScalingFactor();
            lineChartData[1] = RandomNumberGenerator.randomScalingFactor();
            lineChartData[2] = RandomNumberGenerator.randomScalingFactor();
            lineChartData[3] = RandomNumberGenerator.randomScalingFactor();
            lineChartData[4] = RandomNumberGenerator.randomScalingFactor();
            lineChartData[5] = RandomNumberGenerator.randomScalingFactor();
            lineChartData[6] = RandomNumberGenerator.randomScalingFactor();

            colorString = "rgba(" + RandomNumberGenerator.randomColorFactor() + "," + RandomNumberGenerator.randomColorFactor() + "," + RandomNumberGenerator.randomColorFactor() + ",.3)";
            */    
    }
    }

    public class CriticalDataUpdate
    {

        // Singleton instance
        private readonly static Lazy<CriticalDataUpdate> _instance = new Lazy<CriticalDataUpdate>(() => new CriticalDataUpdate());
        // Send Data every 5 seconds
       // readonly int _updateInterval = SettingsConfig.set.updateInterval;
        //Timer Class
        private Timer _timer;
        private volatile bool _sendingChartData = false;
        private readonly object _chartUpateLock = new object();
        LineChart lineChart = new LineChart();
       // public static String hubname;
        private CriticalDataUpdate()
        {

        }

        public static CriticalDataUpdate Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        // Calling this method starts the Timer
        public void GetChartData(int updatePeriod)
        {
            _timer = new Timer(ChartTimerCallBack, null, updatePeriod, updatePeriod);
        }
        private void ChartTimerCallBack(object state)
        {
            if (_sendingChartData)
            {
                return;
            }
            lock (_chartUpateLock)
            {
                if (!_sendingChartData)
                {
                    _sendingChartData = true;
                    SendChartData();
                    _sendingChartData = false;
                }
            }
        }

        private void SendChartData()
        {
           // try
           // {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(SettingsConfig.settings.fileLocation))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadLine();
                    String[] values = line.Split(new string[] { "---" }, StringSplitOptions.None);
                    lineChart.SetLineChartData(values);
                    GetAllClients().All.UpdateChart(lineChart);

                }
        //    }
           /* catch (Exception e)
            {
                String[] values = { "10" };
                lineChart.SetLineChartData(values);
                pieChart.SetPieChartData();
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(), "alertMessage", @"alert('" + e.Message + "')", true);
                GetAllClients().All.UpdateChart(lineChart, pieChart);

            }*/


            /*  lineChart.SetLineChartData(values);
              pieChart.SetPieChartData();
              GetAllClients().All.UpdateChart(lineChart, pieChart);
              */
        }

        private static dynamic GetAllClients()
        {
            return GlobalHost.ConnectionManager.GetHubContext<Hubs.CriticalHub>().Clients;
        }

    }
}