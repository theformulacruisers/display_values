﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Hubs
{   
    [HubName("NonCriticalHub")]
    public class NonCriticalHub : Hub
    {
        // Create the instance of ChartDataUpdate
        private readonly NonCriticalDataUpdate _ChartInstance;
        public NonCriticalHub() : this(NonCriticalDataUpdate.Instance) { }

        public NonCriticalHub(NonCriticalDataUpdate ChartInstance)
        {
            _ChartInstance = ChartInstance;
        }

        public void InitChartData()
        {
            //Show Chart initially when InitChartData called first time
            LineChart lineChart = new LineChart();
            string[] array = { "0" };
            lineChart.SetLineChartData(array);
            Clients.All.InitChart(SettingsConfig.settings.nonCriticalHub.charts);

            //Call GetChartData to send Chart data every 5 seconds
            _ChartInstance.GetChartData(SettingsConfig.settings.nonCriticalHub.updatePeriod);

        }
}
}