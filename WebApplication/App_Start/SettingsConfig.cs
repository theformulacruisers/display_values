﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Hosting;
using WebApplication.Models;
using System.Xml;

namespace WebApplication
{
    public static class SettingsConfig
    {
        // public static String SETTINGS_PATH = HostingEnvironment.MapPath(@"~/App_Data/settings.txt");
        public static String SETTINGS_PATH = HostingEnvironment.MapPath(@"~/App_Data/settings.xml");
        public static String CHARTS_PATH = HostingEnvironment.MapPath(@"~/App_Data/chart.xml");

        public static Settings settings { get; set; }
        public static void SettingsStart()
        {

            String file_Path = HostingEnvironment.MapPath(@"~/test.txt");
            int updatePeriodCrit = 1000;
            int updatePeriodNon = 1000;
            String nameCritical = "Critical Values";
            String nameNonCritical = " Secondary Values";
            if (!File.Exists(SETTINGS_PATH))
            {
                XmlDocument doc = new XmlDocument();
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);
                XmlElement main = doc.CreateElement("Settings");
                XmlElement filelocation = doc.CreateElement("FileLocation");
                filelocation.InnerText = file_Path;
                XmlElement hubs = writeHubs(doc, updatePeriodCrit, updatePeriodNon);
                main.AppendChild(filelocation);
                main.AppendChild(hubs);
                doc.AppendChild(main);
                doc.Save(SETTINGS_PATH);
            }
            else if (File.Exists(SETTINGS_PATH))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(SETTINGS_PATH);
                file_Path = doc.GetElementsByTagName("FileLocation")[0].ChildNodes.Item(0).InnerText.Trim();
                nameCritical = doc.GetElementsByTagName("Name")[0].ChildNodes.Item(0).InnerText.Trim();
                nameNonCritical = doc.GetElementsByTagName("Name")[1].ChildNodes.Item(0).InnerText.Trim();
                updatePeriodCrit = Int32.Parse(doc.GetElementsByTagName("UpdatePeriod")[0].ChildNodes.Item(0).InnerText.Trim());
                updatePeriodNon = Int32.Parse(doc.GetElementsByTagName("UpdatePeriod")[1].ChildNodes.Item(0).InnerText.Trim());
            }
            settings = new Settings(file_Path, new HubSettings(nameCritical,updatePeriodCrit), new HubSettings(nameNonCritical, updatePeriodNon));
            readChartFiles();
        }
        public static void readChartFiles()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(CHARTS_PATH);
            XmlNodeList charts = doc.GetElementsByTagName("Chart");
            foreach (XmlNode chart in charts)
            {
                String id = chart["id"].InnerText;
                String hubname = chart["hub"].InnerText;
                int maxPoints = Int32.Parse(chart["maxPoints"].InnerText);
                String type = chart["type"].InnerText;
                XmlNodeList datasets = chart["datasets"].ChildNodes;
                List<Datasets> datasets2 = new List<Datasets>();
                foreach (XmlNode data in datasets)
                {
                    String label = data["label"].InnerText;
                    String backgroundColor = data["backgroundColor"].InnerText;
                    String secondaryColor = data["secondaryColor"].InnerText;
                    int pointRadius = Int32.Parse(data["pointRadius"].InnerText);

                    datasets2.Add(new Datasets(label, backgroundColor, secondaryColor, pointRadius, null));
                }
                Options options = new Options();
                XmlNode yAxes = chart["options"].GetElementsByTagName("yAxes").Item(0);
                options.yAxes.minimum = Int32.Parse(yAxes["min"].InnerText);
                options.yAxes.maximum = Int32.Parse(yAxes["max"].InnerText);
                options.yAxes.stepSize = Int32.Parse(yAxes["stepSize"].InnerText);

                settings.getHubByName(hubname).charts.Add(new Chart(id,type,datasets2,maxPoints,options));
            }
        }
        public static void update(String fileLocation, int periodCrit, int periodNonCrit)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(SETTINGS_PATH);
            doc.GetElementsByTagName("FileLocation")[0].ChildNodes.Item(0).InnerText = fileLocation;
            doc.GetElementsByTagName("UpdatePeriod")[0].ChildNodes.Item(0).InnerText = periodCrit+"";
            doc.GetElementsByTagName("UpdatePeriod")[1].ChildNodes.Item(0).InnerText = periodNonCrit + "";
            doc.Save(SETTINGS_PATH);
            settings.fileLocation = fileLocation;
            settings.criticalHub.updatePeriod = periodCrit;
            settings.nonCriticalHub.updatePeriod = periodNonCrit;
           /* using (StreamWriter sw = new StreamWriter(SETTINGS_PATH, false))
            {
                sw.WriteLine("FILE_LOCATION|" + fileLocation);
                sw.WriteLine("UPDATE_INTERVAL|" + updateInterval);
                sw.Close();
            }*/
        }

        private static XmlElement writeHubs(XmlDocument doc, int period1, int period2) {
            XmlElement main = doc.CreateElement("hubs");
            XmlElement hub1 = doc.CreateElement("Hub");
            hub1.SetAttribute("id", "CriticalHub");
            XmlElement name1 = doc.CreateElement("Name");
            name1.InnerText = "Critical Hub";
            XmlElement updatePeriod1 = doc.CreateElement("UpdatePeriod");
            updatePeriod1.InnerText = period1+"";
            hub1.AppendChild(name1);
            hub1.AppendChild(updatePeriod1);
            main.AppendChild(hub1);

            XmlElement hub2 = doc.CreateElement("Hub");
            XmlElement name2 = doc.CreateElement("Name");
            hub2.SetAttribute("id", "NonCriticalHub");
            name2.InnerText = "Non Critical Hub";
            XmlElement updatePeriod2 = doc.CreateElement("UpdatePeriod");
            updatePeriod2.InnerText = period2 + "";
            hub2.AppendChild(name2);
            hub2.AppendChild(updatePeriod2);
            main.AppendChild(hub2);
            return main;
        }
    }
}
