﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Timers;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        public Settings set;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Settings()
        {
            
            return View(SettingsConfig.settings);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Settings([Bind(Include = "fileLocation,criticalHub,nonCriticalHub")] Settings set)
        {
            if (ModelState.IsValid)
            {
                SettingsConfig.settings.fileLocation = set.fileLocation;
                SettingsConfig.update(set.fileLocation, set.criticalHub.updatePeriod, set.nonCriticalHub.updatePeriod);
                return RedirectToAction("Index");
            }
            return View(set);
        }

    }
}