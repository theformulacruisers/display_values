﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Hubs;

namespace WebApplication.Models
{
    public class Settings
    {
        public String fileLocation { get; set; }
        public HubSettings criticalHub { get; set; }
        public HubSettings nonCriticalHub { get; set; }
        public Settings()
        {

        }

        public Settings(String fileLocation, HubSettings critical, HubSettings nonCritical)
        {
            this.fileLocation = fileLocation;
            this.criticalHub = critical;
            this.nonCriticalHub = nonCritical;
        }

        public void SetFileLocation(String fileLocation)
        {
            this.fileLocation = fileLocation;
        }

        public HubSettings getHubByName(String name){
          if(criticalHub.name == name){
            return criticalHub;
          }else if (nonCriticalHub.name == name){
            return nonCriticalHub;
          }
            return null;
        }

    }
}
