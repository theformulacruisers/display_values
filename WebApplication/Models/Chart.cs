﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Axes {
      public int suggestedMin {get;set;}
      public int suggestedMax {get;set;}
      public int minimum {get;set;}
      public int maximum {get;set;}
      public int stepSize{get;set;}
        public Axes()
        {
            this.suggestedMin = 0;
            this.minimum = -200;
            this.maximum = 200;
            this.suggestedMax = 100;
        }
    }
    public class Options {
      public Boolean responsive {get;set;}
      public Boolean animation {get;set;}
      public Axes yAxes {get;set;}
      public Axes xAxes {get;set;}
        public Options()
        {
            this.responsive = true;
            this.animation = true;
            this.yAxes = new Axes();
            this.xAxes = new Axes();
        }
    }
    public class Datasets
    {
        public float[] data { get; set; }
        public String label { get; set; }
        public String backgroundColor { get; set; }
        public String secondaryColor { get; set; }
        public int pointRadius { get; set; }
        public Datasets()
        {
        }

        public Datasets(String label, String backgroundColor, String secondaryColor, int pointRadius, float []data)
        {
            this.label = label;
            this.backgroundColor = backgroundColor;
            this.secondaryColor = secondaryColor;
            this.pointRadius = pointRadius;
            this.data = data;
        }
    }
    public class Chart
    {
        public String id {get;set;}
        public String type { get; set; }
        public int maxPoints { get; set; }
        public List<Datasets> datasets { get; set; }
        public Options options {get;set;}
        public Chart(){
          this.datasets = new List<Datasets>();
        }

        public Chart(String id, String type, List<Datasets> datasets, int maxPoints, Options options){
          this.id = id;
          this.type = type;
          this.maxPoints = maxPoints;
          this.datasets = datasets;
          this.options = options;
        }
    }
}
