﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class HubSettings
    {
        public String name { get; set; }
        public int updatePeriod { get; set; }
        public List<Chart> charts { get; set; }

        public HubSettings()
        {

        }

        public HubSettings(String name, int updatePeriod)
        {
            this.name = name;
            this.updatePeriod = updatePeriod;
            this.charts = new List<Chart>();
        }
    }
    
}